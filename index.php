<?php

// require composer autoload
require_once __DIR__ . '/vendor/autoload.php';

use TyPackagist\Hello;
use TyPackagist\Tests\ContentTest;

echo "index page";

// Runs all the files under the folder (src)
echo Hello::world();

// test page run
echo ContentTest::getHealth();