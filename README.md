
# Php Composer Paketi Oluşturma 

Php ile composer paketi nasıl oluşturulur adım adım uygulayacağız.

## Dizin Yapısı

TyPackagist

TyPackagist/src

TyPackagist/src/Hello.php

TyPackagist/composer.json

TyPackagist/index.php

## Step 1
İlk önce dizin oluşturulur. Örneğin benim paketimin adı TyPackagist olacak.
Bu isimde bir klasör oluşturulur. Bu paketin kök dizinidir.

Root Folder name: "TyPackagist"

## Step 2

İlk önce composer.json dosyası oluşturmalıyız. Bu paketimizi tanımlamamızı sağlar. Paket hakkında tüm bilgilere burada yer verirerek tanımlamaları gerçekleştiririz.

Dizinde iken terminal den "composer install" demek yeterli. Böylece composer.json oluşturmada bize yardımcı olacak adımları takip ederek composer.json dosyasını oluşturmuş oluruz. Bu dosya daha sonra editörde açarak düzenleyebiliriz.

```sh
$ composer install
```

Yada manuel olarak composer.json dosyası oluşturup editörle içeriğini düzenleyip/oluşturabilirsiniz.
```sh
{
    "name": "tahsinyuksel/TyPackagist",
    "description": "php base packagist example with composer psr-4",
    "license": "MIT",
    "authors": [
        {
            "name": "tahsn yüksel",
            "email": "info@tahsinyuksel.com"
        }
    ],
    "minimum-stability": "dev",
    "require": {
        "php": ">=5.3.0"
    },
    "autoload": {
        "psr-4": {
            "TyPackagist\\": "src/"
        }
    }
}
```
name: yayıncı / paket adı

authors: sahibi

require: bu paket için gereklilikleri belirtiyoruz. Örn: php 5.3.0 versiyonu ve üzeri olması gerekiyor.

autoload: dosyaların psr-4 standardına göre namespace ler ile tanımlayarak otomatik yüklenmesini sağlar inculde etmesini sağlar. En güzel kısımlardan biri bu.

## Step 3

src dizinin içinde paketimize ait dosyalarımızı oluşturabiliriz.

paketAdı/src

Örneğin: TyPackagist/src 

Hello.php adında bir dosya oluşturalım. 

Dikkat: burada önemli nokta namespace tanımlaması. Paket adımız olan namespace i sayfanın en üzerinde yer vereceğiz.

```sh
<?php 

namespace TyPackagist;

class Hello
{
    public static function world()
    {
        return 'Hello World, Composer Psr-4!';
    }
}
```

## Step 4

artık kök dizinde bir index.php dosyası oluşturarak test psr-4 autoloaderı test edebiliriz. Bunun için composer ile oluşturulan autoloader.php dosyasının include edilmesini sağlamamız gerekiyor. Artık namespace ler ile belirttiğimiz dosyalarımızı composer kendi otomatik tanıyıp bizim yerimize inculude edecektir.

Kök dizinde index.php dosyası oluşturulur. 
index.php

```sh
<?php
// require composer autoload
require_once __DIR__ . '/vendor/autoload.php';

// hello class
use TyPackagist\Hello;

// Runs all the files under the folder (src)
echo Hello::world();
```

## Step 5
Packagist Tanımlama

https://packagist.org

Packagist hesabı olması gerekiyor. Bir hesabınız var ise veya hesap oluşturduktan sonra paketimizin kaynağını(github, bitbucket gibi) "Submit a Package" ile belirtiyoruz.

Bu aşamada kaynağın doğrulaması yapılacak ve başarılı ise paket Packagist aracılığıyla composer ile kurulabilir hale gelecektir.

İşlem bukadar.

